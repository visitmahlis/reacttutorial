import logo from './logo.svg';
import './App.css';
import ReducerHook from './components/hooks/reducer/ReducerHook';
import ContextHome from './components/context';
import HOCHome from './components/hoc';
import ClassComponentHome from './components/classComponents/ClassComponentsHome';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <ReducerHook></ReducerHook> */}
        {/* <ContextHome></ContextHome> */}
        {/* <HOCHome></HOCHome> */}
        <ClassComponentHome/>
      </header>
    </div>
  );
}

export default App;
