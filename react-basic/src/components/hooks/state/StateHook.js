import React, {useState} from "react";

const StateHook = () => {
    const [counter, setCounter]  = useState(0);
    const [renderData, setRenderData] = useState(false);
    const [userName, setUserName] = useState(null);
    const [userEmail, setUserEmail] = useState(null);
    const [userMobile, setUserMobile] = useState(null);
    const [userPassword, setUserPassword] = useState(null);


    const increaseCount = () =>{
        setCounter(counter+1);
    };

    const onChange = (event) => {
        setUserName(event.target.value);
        setUserEmail(event.target.value);
        setUserMobile(event.target.value);
        setUserPassword(event.target.value);
    };

    const displayData = () => {
        setRenderData(true);
    };
    return(
        <div>
            <div>{counter}</div>
            <button onClick={increaseCount}>count</button> 

            <div><input type="text" placeholder="Enter your name..." onChange={onChange}></input></div>
            <div><input type="email" placeholder="Enter your email..." onChange={onChange}></input></div>
            <div><input type="tel" placeholder="Enter your mobile no..." onChange={onChange}></input></div>
            <div><input type="password" placeholder="Enter your password..." onChange={onChange}></input></div>
            <div><button onClick={displayData}>display user data</button></div>
            {renderData && 
            <p>
                <div>{userName} </div>
                <div>{userEmail} </div>
                <div>{userMobile} </div>
                <div>{userPassword} </div>
            </p>
            }

        </div>
    );
};

export default StateHook;