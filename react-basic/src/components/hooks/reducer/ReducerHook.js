import React, {useReducer} from "react";

const reducer = (state, action) => {
    switch(action.type){
        case "INCREMENT":
            return {
                    counter : state.counter+1,
                    renderData : state.renderData,
                }
        case "RENDER" :
            return {
                    counter : state.counter,
                    renderData : !state.renderData,
                }
        default :
                return {state}
            }
        };

const ReducerHook = () => {

    const [state, dispatch] = useReducer(reducer, {counter : 0, renderData : false});

    return(
        <div>
            <div>{state.counter}</div>
            <button onClick={()=>{ dispatch({type : "INCREMENT"})}}>count</button> 
            <div><button onClick={()=>{dispatch({type : "RENDER"})}}>display test</button>
            </div>
            {state.renderData && <p>Hello World</p>}

        </div>
    );
};

export default ReducerHook;