import React from "react";
import StateHook from "./state/StateHook";
import ReducerHook from "./reducer/ReducerHook";
import EffectHook from "./effect/EffectHook";
import RefHook from "./ref/RefHook";

const Hooks = () =>{
    return(
        <RefHook></RefHook>
    );
};

export default Hooks;