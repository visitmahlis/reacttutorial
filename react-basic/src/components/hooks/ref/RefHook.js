import React, {useRef} from "react";

// this hook is used to access and manupulate DOM element

const RefHook = () => {
    return(
        <div>
            <div><h1>Suraj</h1></div>
            <div><input type="text"/></div>
            <div><button>change name</button></div>
        </div>
    );
};

export default RefHook;