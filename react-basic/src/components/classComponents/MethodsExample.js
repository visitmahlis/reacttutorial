import React from "react";

class MethodsExample extends React.Component{
    constructor(){
        super();
        this.callMethod = this.callMethod.bind(this);
    }
    callMethod = (event) =>{
        console.log('Button clicked!!!!');
    };

    render(){
        return(
            <div>
                <button onClick={this.callMethod}>Click Me!!!</button>
            </div>
        );
    };
}

export default  MethodsExample;