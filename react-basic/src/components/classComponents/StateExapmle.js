import React from "react";

class StateExample extends React.Component{
    constructor(){
        
        super();

        this.state = {
            count: 0
        };

        this.changeState = this.changeState.bind(this);
    }

    changeState = () =>{
        this.setState({count: this.state.count+1});
    }

    render(){
        return(
            <div>
                 <div>count: {this.state.count}</div>
                 <div><button onClick={this.changeState}>Click Me!!!</button></div>
            </div>
        );
    };
};

export default StateExample;
