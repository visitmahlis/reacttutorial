import React, {Component} from "react";

class PropsExample extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <div>First Name: {this.props.firstName}</div>
                <div>Last Name: {this.props.lastName}</div>
            </div>
        );
    };
}

export default PropsExample;