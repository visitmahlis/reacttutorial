import React, {Component} from "react";
import DeclareComponent from "./DeclareComponent";
import MethodsExample from "./MethodsExample";
import PropsExample from "./ProprsExample";
import StateExample from "./StateExapmle";

class ClassComponentHome extends Component {
    render(){
        return(
            //  <DeclareComponent/>
            // <MethodsExample/>
            // <PropsExample firstName="Suraj" lastName="Mahli"/>
            <StateExample/>
        );
    }
}

export default ClassComponentHome;