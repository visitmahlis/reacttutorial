import React, {useState} from 'react';
import Child from './Child';
export const MyContext = React.createContext();

export default function Parent() {
    const [name, setName] = useState("Ronaldo");
    const [score, setScore] = useState(0);
    const countScore = ()=>{
        setScore(score+1);
    };
    const contextValue = {
        contextName: name,
        contextScore: score,
        contextHandler: countScore,
    };
  return (
    <div>
        <MyContext.Provider value={contextValue}>
            <Child/>
        </MyContext.Provider>
    </div>
  )
};
