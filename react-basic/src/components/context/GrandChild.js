import React from 'react';
import { MyContext } from './Parent';

export default function GrandChild() {
  return (
    <div>
        <div>Grand Child</div>
        <MyContext.Consumer>
            {
                (data) => <div>Name : {data.contextName}, Score: {data.contextScore}
                    <button onClick={data.contextHandler}>Strike!!</button>
                    </div>
                
            }
            
        </MyContext.Consumer>
    </div>
  );
};
