import React from 'react'
import Coin from './coin/Coin'
import Counter from './counter/Counter'

export default function WithoutReduxHome() {
  return (
    <div>
      <Counter></Counter>
      <Coin></Coin>
    </div>
  )
}
