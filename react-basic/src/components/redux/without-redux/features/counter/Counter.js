import React, {useState} from 'react'

export default function Counter() {
    
    const [count, setCount] = useState(0);

    const increase = ()=>{
        setCount(count+1)
    };

    const decrease = ()=>{
        setCount(count-1)
    };

  return (
    <div>
        <div><button onClick={increase}>+</button></div>

        <div><span>Count :{count}</span></div>

        <div><button onClick={decrease}>-</button></div>

    </div>
  )
}
