import React from 'react'
import Coin from './coin/Coin'
import Counter from './counter/Counter'

export default function WithReduxHome() {
  return (
    <div>
      <Counter></Counter>
      <Coin></Coin>
    </div>
  )
}
