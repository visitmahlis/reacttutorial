import React from 'react';
import { useSelector } from "react-redux";

export default function 
() {
  const count = useSelector((state)=>state.counter.count);
  return (
    <div>
        <div><span>Coin :{count}</span></div>
    </div>
  )
}
