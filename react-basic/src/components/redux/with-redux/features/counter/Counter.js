import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from './counterSlice';


export default function Counter() {
    
    const count = useSelector((state)=>state.counter.count);
    const dispatch = useDispatch();


  return (
    <div>
        <div><button onClick={()=>dispatch(increment())}>+</button></div>

        <div><span>Count :{count}</span></div>

        <div><button onClick={()=>dispatch(decrement())}>-</button></div>

    </div>
  )
}
