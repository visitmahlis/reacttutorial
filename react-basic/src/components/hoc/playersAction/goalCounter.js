import React, { Component } from 'react';

 const Goal = (Player, strikeRate) =>{
    class Play extends Component{
        state = {
            goal : 0
        };

        handleEvent = () =>{
            this.setState({goal : this.state.goal+strikeRate});
        };
        render(){
            return <Player 
                        hocGoal={this.state.goal}
                        hocHandleEvent={this.handleEvent}
                        {...this.props}
                    />
        }
    }
   return Play;
};

export default Goal;