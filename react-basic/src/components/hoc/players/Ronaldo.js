import React, { Component } from 'react';
import Goal from "../playersAction/goalCounter";

class Ronaldo extends Component{
    render(){
        return(
            <div>
                <div onMouseOver={this.props.hocHandleEvent}>Name : {this.props.playerName}</div>
                <div>Position : {this.props.position}</div>
                <div>Score : {this.props.hocGoal}</div>
            </div>
        );
    }
};

export default Goal(Ronaldo, 2);