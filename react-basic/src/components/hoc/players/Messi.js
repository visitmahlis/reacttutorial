import React, { Component } from 'react';
import Goal from '../playersAction/goalCounter';

class Messi extends Component {
    render(){
        return(
            <div>
                <div onMouseOver={this.props.hocHandleEvent}>Name : {this.props.playerName}</div>
                <div>Position : {this.props.position}</div>
                <div>Score : {this.props.hocGoal}</div>
            </div>
        );
    }
}

export default Goal(Messi, 1);