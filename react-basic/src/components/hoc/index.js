import React, { Component } from 'react';
import Messi from './players/Messi';
import Ronaldo from './players/Ronaldo';

class HOCHome extends Component{
    render(){
        return(
            <div>
                <Ronaldo playerName="Ronaldo" position="Striker"></Ronaldo>
                <Messi playerName="Messi" position="Striker"></Messi>
            </div>
        );
    }
};

export default HOCHome;